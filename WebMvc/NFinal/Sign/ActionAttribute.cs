﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NFinal.Sign
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ActionAttribute : Attribute
    {
        public ActionAttribute(ResultType type)
        { }
    }
}