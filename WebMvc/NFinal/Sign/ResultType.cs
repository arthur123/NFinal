﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NFinal.Sign
{
    public enum ResultType
    {
        View,
        Empty,
        Redirect,
        Json,
        JavaScript,
        Content,
        FileContent,
        FilePath,
        FileStream
    }
}