﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFinal.Sign
{
    public enum ValidType
    {
        phone,
        fax,
        email,
        postcode,
        empty,
        idcard,
        date,
        time
    }
}