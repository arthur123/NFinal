﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NFinal.Sign
{
    [AttributeUsage(AttributeTargets.Parameter)]
    public class NotNullAttribute : ArgumentValidationAttribute
    {
        public NotNullAttribute()
        {
        }
    }
}