﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NFinal.Sign
{
    [AttributeUsage(AttributeTargets.Parameter)]
    public class InRangeAttribute : ArgumentValidationAttribute
    {
        public InRangeAttribute(int min, int max)
        {
        }
    }
}