﻿using System;
//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :UserControl.cs
//        Description :用户控件父类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System.Collections.Generic;
using System.Web;

namespace NFinal
{
    public class UserControl:System.Web.UI.UserControl
    {
        public delegate void __Render__<T>(T obj);
    }
}