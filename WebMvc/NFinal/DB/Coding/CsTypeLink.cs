﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//
//        Application:NFinal MVC framework
//        Filename :CsTypeLink.cs
//        Description : harp所对应的数据库类型,数据库参数枚举类型
//
//        created by Lucas at  2015-6-30`
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.IO;

namespace NFinal.DB.Coding
{
    /// <summary>
    /// sharp所对应的数据库类型,数据库参数枚举类型
    /// </summary>
    public class CsTypeLink
    {
        public string csharpType;
        public string sqlDbGetType;
        public string sqlDbGetMethod;
        public string sqlType;
        public string sqlDbType;
    }
}