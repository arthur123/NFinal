﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :Public.cs
//        Description :公用控制器方法类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;

namespace {$project}.{$app}.Controllers.Common
{
    public class Public: Controller
    {
        public void Success(string message,string url,int second)
        {
            View("Public/Success.aspx");
        }
        public void Error(string message, string url, int second)
        {
            View("Public/Success.aspx");
        }
    }
}