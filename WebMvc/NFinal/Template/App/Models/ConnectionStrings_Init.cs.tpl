﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :ConnectionStrings.cs
//        Description :数据库连接字符串
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;

namespace {$project}.{$app}.Models
{
    public class ConnectionStrings
    {
        public static string Common = @"Data Source=|DataDirectory|Common.db;Pooling=true;FailIfMissing=false";
    }
}