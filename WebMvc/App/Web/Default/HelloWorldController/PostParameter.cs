﻿using System;
using System.Collections.Generic;
using System.Web;
using NFinal.Sign;

namespace WebMvc.App.Web.Default.HelloWorldController
{
    public class PostParameterAction  : Controller
	{
		public PostParameterAction(System.IO.TextWriter tw):base(tw){}
		public PostParameterAction(string fileName) : base(fileName) {}
        #region 输出
        //输出文本
        
        //输出HTML
        
        //输出Json
        
        #endregion
        #region 输入
        //URL
        /// <summary>
        /// ajax
        /// </summary>
        /// <param name="name">名称</param>
        [Action(ResultType.Content)]
        public void Parameter([Simple(ValidType.email)]string name,[InRange(1,5)]string pwd)
        {
            WriteLine("http://localhost/App/HelloWorldController/Parameter/name/NFinal.htm");
            Write("您好:");
            Write(name);
        }
        
        
        //POST提交
        public void PostParameter(string say)
        {
            Write(say);
        }
        #endregion
    }
}