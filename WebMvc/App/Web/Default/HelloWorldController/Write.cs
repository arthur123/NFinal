﻿using System;
using System.Collections.Generic;
using System.Web;
using NFinal.Sign;

namespace WebMvc.App.Web.Default.HelloWorldController
{
    public class WriteAction  : Controller
	{
		public WriteAction(System.IO.TextWriter tw):base(tw){}
		public WriteAction(string fileName) : base(fileName) {}
        #region 输出
        //输出文本
        public void Write()
        {
            string words = "Hello World!";
            Write(words);
        }
        //输出HTML
        
        //输出Json
        
        #endregion
        #region 输入
        //URL
        /// <summary>
        /// ajax
        /// </summary>
        /// <param name="name">名称</param>
        [Action(ResultType.Content)]
        public void Parameter([Simple(ValidType.email)]string name,[InRange(1,5)]string pwd)
        {
            WriteLine("http://localhost/App/HelloWorldController/Parameter/name/NFinal.htm");
            Write("您好:");
            Write(name);
        }
        
        
        //POST提交
        
        #endregion
    }
}