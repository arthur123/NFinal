﻿using System;
using System.Collections.Generic;
using System.Web;
using NFinal.Sign;

namespace WebMvc.App.Web.Default.HelloWorldController
{
    public class WriteJsonAction  : Controller
	{
		public WriteJsonAction(System.IO.TextWriter tw):base(tw){}
		public WriteJsonAction(string fileName) : base(fileName) {}
        #region 输出
        //输出文本
        
        //输出HTML
        
        //输出Json
        public void WriteJson()
        {
            //代码,消息
            AjaxReturn(1, "Hello World!");
        }
        #endregion
        #region 输入
        //URL
        /// <summary>
        /// ajax
        /// </summary>
        /// <param name="name">名称</param>
        [Action(ResultType.Content)]
        public void Parameter([Simple(ValidType.email)]string name,[InRange(1,5)]string pwd)
        {
            WriteLine("http://localhost/App/HelloWorldController/Parameter/name/NFinal.htm");
            Write("您好:");
            Write(name);
        }
        
        
        //POST提交
        
        #endregion
    }
}